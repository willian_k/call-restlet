callRestlet( id , [deployid] , data , [nsconfig_opts] )
---

Calls a netsuite restlet (from node.js) using `nsconfig` preferences for account/user/password, etc.