var nsconfig = require('nsconfig')
var request = require('request')

module.exports = main
function main( scriptid , args0, args1, args2 ) {

    var deployid, data, nsconf_opts;
    if (!Number(args0)) {
        deployid = 1
        data = args0
        nsconf_opts = args1
    } else {
        deployid = args0
        data = args1
        nsconf_opts = args1
    }
    if (!scriptid) throw Error('scriptid missing')
    if (typeof data !== 'object') throw Error('data missing or not an object')

    nsconf_opts = nsconfig(nsconf_opts||{});
    var toRequest = _requestOpts(nsconf_opts, scriptid, deployid);
    toRequest.json = data;
    return new Promise( (resolve, reject) => {
        request(toRequest, (err, resp, body) => {
            if (err) return reject(err)
            if (typeof body == 'string') body = JSON.parse(body.replace(new RegExp("\\\\","g"),""))
            if (body && body.error) return reject(body.error)
            resolve(body)
        });
    })

}

function _requestOpts (params, script, deployment) {
    'use strict'

    var nlauthRolePortion = ( params.role ) ? `,nlauth_role=${params.role}` : '',
        server = `https://rest.${params.realm}/app/site/hosting/restlet.nl`;

    var o = {
        url: server,
        qs: {
            script: script,
            deploy: deployment
        },
        method : 'POST' ,
        headers: {
            authorization: `NLAuth nlauth_account=${params.account},nlauth_email=${params.email},nlauth_signature=${params.password}${nlauthRolePortion}` ,
            'content-type': 'application/json'
        }
    };

    return o;
}